# Premier projet java
Un premier projet java généré avec l'archetype maven quickstart

## Exo conditions + variable  (App.java => méthode conditionExplanation)
1. Créer une variable avec un age dedans, mettez la valeur que vous voulez dedans
2. Créer une condition qui affichera quelque chose si l'age est supérieur à 18 
3. Créer une autre variable ageLimit et faire en sorte de modifier la condition précédente pour comparer l'age et l'ageLimit
4. Créer une variable contenant une chaîne de caractère et faire une condition qui dit que si c'est votre prénom dans la variable, ça affiche "bonjour " suivit du contenu de la variable, sinon faire que ça affiche "casse toi"
5. Créer une autre condition qui va vérifier le nombre de caractère dans la variable prenom, et s'il y a autant de caractère que la valeur de age, alors ça met un message
6. Faire une variable qui va contenir un int puis faire une condition qui affichera even ou odd selon si le nombre est pair ou impair

## [Exo Calculator](src/main/java/co/simplon/promo16/oop/Calculator.java)
1. Créer une nouvelle classe Calculator dans le package oop
2. Dans cette classe, ajouter une méthode/fonction add qui va attendre 2 arguments de type int a et b
3. Dans la méthode add, faire un sysout du résultat de a + b
4. Dans le main, instancier la classe Calculator et appeler sa méthode add plusieurs fois avec des valeurs différentes pour voir si ça marche 
5. Dans la classe Calculator, créer une nouvelle méthode calculate qui va attendre int a, int b et String operator
6. Faire des if dans la méthode qui va vérifier si operator contient + ou - ou * ou / et faire un return de la bonne opération selon l'operator
7. Créer une nouvelle méthode isEven dans Calculator qui va renvoyer true ou false selon si le nombre donné en argument est pair ou pas

## [Exo Validator](src/main/java/co/simplon/promo16/oop/StringValidator.java)
1. Créer une nouvelle classe StringValidator dans le package oop
2. Dans cette classe, créer une méthode isCorrectLength qui attendra une String et renverra true ou false si l'argument fait entre 2 et 32 caractères 
3. Ptit bonus, pourquoi pas faire en sorte que le nombre de caractères min et max soient des propriétés de la classe StringValidator
4. Créer une méthode haveTrailingSpaces qui attendra une String et renverra true si la String commence ou termine par un espace et false si ce n'est pas le cas
5. Créer une méthode haveSpecialChars qui attendra une String et renverra true si elle contient un _ ou un - ou un ! et renverra false sinon
6. Enfin, faire une méthode validate, qui attendra une String et va passer cette string à toutes les méthodes précédentes et qui renverra true si la length est correct, qu'il n'y a pas de trailing spaces et qu'il n'y a pas de special chars

## [Exo Person](src/main/java/co/simplon/promo16/oop/Person.java)
1. Créer une classe oop.Person qui aura comme propriété un name, un firstName, les deux en String 
2. Créer un constructeur pour cette classe qui permettra d'assigner le name et le firstName (car ce sont des propriétés qui peuvent difficilement avoir des valeurs par défaut, genre "c'est quoi un nom par défaut ?" bah rien)
3. Faire une méthode toString public qui renverra du String et qui fera un return de "This is a Person. name: [la valeur de name], firstName: [la valeur de firstName]"
4. Dans le main, faire 2 instances différentes de la classe Person, et faire un sysout de chaque instance
5. Rajouter une propriété energy en int à notre Person, et faire en sorte que sa valeur par défaut à l'instanciation soit 100
6. Créer une méthode sleep qui augmente l'energy de la person de 40 mais en faisant en sorte que ça ne dépasse pas les 100 (c'est optimiste, si vous voulez coder un truc réaliste, je vous laisse faire que ça ajoute une valeur random en plus entre -20 et 40) 
7. Créer une méthode socialize qui va attendre en argument une autre Person, et qui va faire un sysout de "Hello [le nom de la person à qui on parle], how are you", et qui va réduire l'energy de 10 (ou plus, selon combien vous pensez que ça enlève d'énergie d’interagir avec des gens)
8. Bonus : Faire que si on a pas l'energy suffisante pour socialize, à la place de Hello, on se roule en boule dans un coin. Faire que dans le socialize, la Person à qui on parle "réponde" un autre sysout selon son energy à elle (genre "I'm fine" si au dessus de 70, "I'm ok" entre 40 et 70 et "I can't even" en dessous de 30)

## [Exo Boucle gentillet](src/main/java/co/simplon/promo16/oop/Looper.java)
1. Créer une variable de type String avec "I love Java"
2. Faire une boucle qui rajoute " very " au bout de la String en question
3. Afficher la String avec " much" à la fin
4. Créer une classe Looper et faire une méthode love dans laquelle vous mettrez le code que vous venez de faire
5. Rajouter un argument à la méthode love pour faire qu'on puise choisir combien de tour va faire la boucle
6. Faire une méthode dans la classe Looper qu'on va appeler multiTable qui va attendre un nombre et qui va utiliser une boucle pour afficher la table de ce nombre 
7. Faire une méthode multiTables (avec un s en plus) qui n'attend pas d'argument et qui va afficher les tables de multiplication de 1 à 10 (là pour le coup, il faudra utiliser une boucle imbriquée pour de vrai) 

## [Exo Person filler](src/main/java/co/simplon/promo16/oop/Person.java)
1. Créer une nouvelle classe Food qui va avoir une propriété name en string et une propriété calories en int
2. Dans la classe oop.Person, créer une nouvelle méthode eat qui attendra en argument une instance de Food
3. Faire que la méthode eat fasse un sysout de "[nom de la personne] eats [nom de la food]" et augmente la propriété energy du nombre de calorie divisé par 10

## [Exo Pyramid](src/main/java/co/simplon/promo16/loop/Looper.java)
1. Déjà créer la méthode pyramid et dedans faire une boucle qui va afficher 5 étoiles les unes en dessous des autres
2. Ensuite, une fois qu'on a ça, essayer de voir s'il y a un pattern logique au niveau du nombre d'étoiles entre chaque étage, apparemment les étoiles font 1 -> 3 -> 5 -> 7 -> 9. Essayer d'afficher cette suite de nombre via la boucle 
3. Une fois qu'on a le bon nombre à chaque tour de boucle, et bien essayer de trouver un moyen d'afficher les étoiles correspondantes à ce nombre (on pourra s'aider des exercices de boucles qu'on a fait hier pour ça)
4. On est pas loin, maintenant, il reste plus qu'à faire le même process que dans le 2 et le 3, mais cette fois ci pour le nombre d'espaces qu'il y a avant les étoiles à chaque étage (4 -> 3 -> 2 -> 1 -> 0, je me demande bien quel est le pattern 🤔 ) 
Bonus : Faire qu'on puisse choisir le nombre d'étages de la pyramide via un argument de la fonction, et aussi pourquoi pas pouvoir choisir un caractère à la place des étoiles 

## [Exo List](src/main/java/co/simplon/promo16/loop/NameOrganizer.java)
1. Créer une classe NameOrganizer qui aura en propriété une liste avec un certain nombre de prénom dedans 
2. Créer une méthode getNameStartingBy qui va attendre une lettre en argument puis qui va afficher en console uniquement les prénoms commençant par cette lettre
3. Plutôt qu'afficher les prénom directement, faire que ça renvoie une nouvelle liste avec que les prénoms ciblés dedans
4. Faire une méthode oddOnly qui fera une boucle sur la liste de prénom et n'affichera dans la console qu'un prénom sur deux
5. Faire méthode StudentS() qui va faire une boucle sur la liste de prénom et affichera uniquement le premier prénom et le dernier en majuscules
6. Dans la classe NameOrganizer, créer une méthode promoMaker() qui va renvoyer une liste de Person
7.  Dans cette méthode, faire une boucle sur les prénoms, et pour chaque prénom, faire une instance de Person et la mettre une liste qui sera return à la fin (la nom de famille sera le même pour tout le monde, sauf si vous voulez faire une deuxième liste)

Bonus : Dans le main, appelé la méthode promoMaker pour récupérer la liste de Person, puis faire une boucle pour faire que chaque student socialize avec le student précédent (sauf le premier, qui va socialize avec le student suivant) 