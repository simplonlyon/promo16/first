package co.simplon.promo16;


/**
 * Ceci est de la JavaDoc, on en mettra généralement sur nos classes et nos
 * méthodes afin d'indiquer ce que font ces dernières. (cette doc pourra être accédée
 * au survol des classes/méthodes, c'est donc assez important de la remplir,
 * c'est personnelement le type de commentaire que je recommande plutôt que des
 * morceaux de commentaire eparpillés dans le code directement, et je déconseille
 * de faire des commentaires ligne par ligne comme j'ai fais en dessous)
 */
public class App {


    /**
     * La fonction static void main est le point d'entrée de l'application java,
     * tout part de lui, il en faut forcément une par projet
     */
    public static void main(String[] args) {
        //Ici, on fait un appel à la méthode variableExplanation et on exécute donc tout ce qu'il y a dedans
        variableExplanation();   
     
        conditionExplanation();
    }

    /**
     * Une méthode dans laquelle on voit les différents types de données primitif
     * ainsi que les différentes opérations arithmétiques et la concaténation.
     */
    @SuppressWarnings("all") //Un truc à pas trop utiliser dans la vraie vie, c'est juste pour qu'il me mette pas les warnings sur ce code
    private static void variableExplanation() {
        /*
            Chaque type de donnée va réserver un espace mémoire particulier
            par exemple un byte fait 8 bits, un double en fait 64.
            On peut éventuellement vouloir optimiser le code en choisissant le type
            suffisant selon quelle genre de données on veut mettre dans une variable
            (exemple, est-il bien utile de stocker un age dans un double ?)

        */
        // Les entiers
        byte firstByte = 127;
        short firstShort = 32000;
        int firstInt = 10;
        long firstLong = 10000000000000L;
        // les décimales
        float firstFloat = 100.3f;
        double firstDouble = 4000.4;

        // char ne représente qu'un seul caractère (on peut aussi le déclarer en int
        // avec la table ascii)
        char firstChar = 'e';
        // Le boolean ne peut être que true ou false
        boolean firstBoolean = false;
        //On change la valeur d'une variable en mettant son nom puis sa nouvelle valeur
        //celle ci doit forcément être du même type que celui avec lequel on l'a initialisée
        firstBoolean = true;

        // Pas primitif mais on s'en sert tout le temps
        String firstString = "Coucou je suis une chaîne de caractère";
        String secondString = "Bloup";

        int resultAdd = 5 + 5;// ça fait 10
        int resultMultiply = 5 * 5;
        float resultDivide = 5 / 2f;
        //Le modulo, une opération qui donne le reste d'une division, on s'en sert surtout pour savoir si un truc est pair ou pas
        int resultModulo = 5 % 3; // genre là ça vaut 2, pasque ya une fois 3 dans 5 et il reste 2

        System.out.println(resultModulo);
        //On peut utiliser les + aussi pour "concaténer" des chaînes de caractère ensemble
        String thirdString = firstString + " " + secondString + 5 + 5;

        firstInt++;// Maintenant ya 10+1 dedans, donc 11
        firstInt += 10;// Maintenant dans firstInt ya 11+10 donc 21
        firstInt -= 2;// Maintenant ya 21-2 donc 19 dedans

        secondString += "blip";// Marche avec la concaténation aussi, contient maintenant "Bloupblip"

        System.out.println(thirdString);
    }


    /**
     * Une méthode dans laquelle on regarde les if et tout
     */
    public static void conditionExplanation() {
        int ageLimit = 18;
        int age = 8;

        if(age >= ageLimit) {
            System.out.println("age supérieur à 18");
        }


        String firstName = "Formateur";
        //if(firstName.equals("Formateur")) {
        if(firstName == "Formateur") {
            System.out.println("Bonjour "+firstName);
        } else {
            System.out.println("casse toi");
        }
        
        //techniquement on peut stocker le résultat d'une comparaison logique dans une variable de type booléenne
        //boolean isSameChar = firstName.length() == age;
        
        
        if(firstName.length() == age) {
            System.out.println("autant de caractère que l'age");
        }


        if (age % 2 == 0) {
            System.out.println("even");
        } else {
            System.out.println("odd");
        }
    }

}
