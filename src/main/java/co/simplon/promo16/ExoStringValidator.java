package co.simplon.promo16;

import co.simplon.promo16.oop.StringValidator;

public class ExoStringValidator {
    

    public static void main(String[] args) {
        StringValidator validator = new StringValidator(2,32);
        // validator.maxLength = 32;
        // validator.minLength = 2;
        System.out.println(validator.isCorrectLength("test")); //true
        System.out.println(validator.haveTrailingSpaces(" test ")); //true
        System.out.println(validator.haveSpecialChars(" te_st ")); //true

        System.out.println(validator.validate("this is correct")); //true
        
    }
}
