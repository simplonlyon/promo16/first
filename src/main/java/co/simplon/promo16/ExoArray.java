package co.simplon.promo16;



import co.simplon.promo16.loop.NameOrganizer;

public class ExoArray {
    
    public static void main(String[] args) {
        // String[] stringArray = {"Ga","Zo","Bu","Meu"};
        // stringArray[0] = "Nouvelle valeur";
        // // stringArray[5] = "test";
        // System.out.println(stringArray[0]);

        // List<String> stringList = Arrays.asList("Ga", "Zo", "Bu", "Meu");
        // stringList.add("Bloup");
        // stringList.set(0, "Nouvelle valeur");

        // for(int i = 0; i < stringList.size(); i++) {
        //     System.out.println(stringList.get(i));
        // }
        
        // for (String item : stringList) {
        //     System.out.println(item);
        // }
        

        NameOrganizer organizer = new NameOrganizer();
        // organizer.getNamesStartingBy("m");
        // System.out.println(organizer.getNamesStartingBy("m"));
        // organizer.oddOnly();
        // organizer.StudentS();

        System.out.println(organizer.promoMaker());
    }
}
