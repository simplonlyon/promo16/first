package co.simplon.promo16;

import co.simplon.promo16.oop.Calculator;

/**
 * J'ai fais plusieurs méthodes main dans ce projet histoire d'organiser les différents
 * exo et sujet ensemble, mais en général, on a qu'un seul main par appli
 */
public class ExoCalculus {

    public static void main(String[] args) {
        Calculator casio = new Calculator();
        
        int result = casio.add(123123,41341241);
        System.out.println( casio.add(10, result) );
        System.out.println(casio.calculate(4, 3, 'b'));
        
        System.out.println(casio.isEven(5));
    }
    
}
