package co.simplon.promo16;

import co.simplon.promo16.oop.Food;
import co.simplon.promo16.oop.Person;

public class ExoPerson {

    public static void main(String[] args) {
        
        Person person1 = new Person("Demel", "John");
        Person person2 = new Person("Bloupy", "Bloup");
        person1.sleep();

        person1.socialize(person2);

        person1.eat(new Food("Banane", 89));

        //Par défaut, quand on sysout une classe, ça appelle ça méthode toString() de manière implicite
        System.out.println(person1);
        System.out.println(person2);

    }
    
}
