package co.simplon.promo16;

import co.simplon.promo16.oop.Dog;

public class ExoDog {
    

    public static void main(String[] args) {
        Dog goodDog = new Dog();
        goodDog.name = "Fido";
        goodDog.age = 3;
        goodDog.age = 2;
        goodDog.breed = "corgi";

        Dog badDog = new Dog();
        badDog.name = "Rex";
        badDog.age = 10;
        badDog.breed = "Nothing";
        
        goodDog.greeting();
        badDog.greeting();

        badDog.bark("Postman");
        badDog.bark("Younglings");
    }
}
