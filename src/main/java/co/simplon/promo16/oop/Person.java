package co.simplon.promo16.oop;

public class Person {
    private String firstName;
    private String name;
    private int energy;

    public Person(String name, String firstName) {
        this.name = name;
        this.firstName = firstName;
        this.energy = 100;
    }
    
    public void sleep() {
        energy+=40;
        if(energy > 100) {
            energy = 100;
        }
    }

    public void socialize(Person person) {
        
        System.out.println("Hello "+person.firstName+", how are you ?");
        energy -= 10;
    }

    public void eat(Food food) {
        System.out.println(firstName + " is eating "+food.getName());
        energy += food.getCalories() / 10;
        //ça, ce serait mieux de l'externaliser vu que je le fais plusieurs fois
        if(energy > 100) {
            energy = 100;
        }
    }

    @Override
    public String toString() {
        return "This is a Person. name: "+name+", firstName: "+firstName+", energy: "+energy;
    }
}


/*
//si on veut augmenter l'energy d'une valeur random entre -20 et 40
 public void incrementEnergy() {
        Random random = new Random();
        energy = energy + random.nextInt(40 - -20) - 20;
    }
    */