package co.simplon.promo16.oop;

public class Calculator {
    

    public int add(int a, int b) {
        return a+b;
    }

    public Integer calculate(int a, int b, char operator) {
        //Une manière de faire la même chose mais avec un switch
        /* switch (operator) {
            case '+':
                return a+b;
            case '-':
                return a-b;
            case '*':
                return a*b;
            case '/':
                return a/b;
            default:
                return 0;
        }*/
        if (operator == '+') {
            return a+b;
        }
        if (operator == '-') {
            return a-b;
        }
        if (operator == '*') {
            return a*b;
        }
        if (operator == '/') {
            return a/b;
        }
        
        return null;
    }

    public boolean isEven(int number) {
        if(number % 2 == 0) {
            return true;
        }
        return false;

        //ça fait pareil, pasque techniquement c'est un opérateur logique qui renvoie un booléen
        // return number % 2 == 0;
    }
}
