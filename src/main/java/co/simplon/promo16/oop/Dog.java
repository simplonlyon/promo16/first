package co.simplon.promo16.oop;
/**
 * Une première classe qui représente un chien.
 */
public class Dog {
    public String name;
    public String breed;
    public int age;

    public Dog(String name, String breed, int age) {
        this.name = name;
        this.breed = breed;
        this.age = age;
    }

    public Dog() {
    }

    public void greeting() {
        System.out.println("Hello, my name is "+name+
        ", I'm a "+breed+" and I'm "+age+" years old. Woof Woof");
    }

    public void bark(String something) {
        System.out.println("Dogs bark on "+something);
    }

    
}
