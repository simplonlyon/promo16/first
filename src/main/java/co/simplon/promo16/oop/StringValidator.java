package co.simplon.promo16.oop;

public class StringValidator {
    private int minLength;
    private int maxLength;
    
    public StringValidator(int minLength, int maxLength) {
        this.minLength = minLength;
        this.maxLength = maxLength;
    }

    public boolean isCorrectLength(String str) {

        if(str.length() > minLength && str.length() < maxLength) {
            return true;
        }
        return false;
    }

    public boolean haveTrailingSpaces(String str) {
        //Pour l'exemple, sans if, ça fait pareil vu que l'opérateur logique renvoie un booléen
        return str.startsWith(" ") || str.endsWith(" ");
    }

    public boolean haveSpecialChars(String str) {
        if(str.contains("-") || str.contains("_") || str.contains("!")) {
            return true;
        }
        return false;
    }

    public boolean validate(String str) {
        if (isCorrectLength(str) && !haveTrailingSpaces(str) && !haveSpecialChars(str)) {
            return true;
        }
        return false;
    }
}
