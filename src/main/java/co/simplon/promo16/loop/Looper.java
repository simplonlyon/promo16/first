package co.simplon.promo16.loop;

public class Looper {

    public String love(int howMuch) {

        String message = "I <3 Java";
        for (int i = 0; i < howMuch; i++) {
            message += " very";
        }
        return message + " much";
    }

    public void multiTable(int number) {

        for (int i = 1; i <= 10; i++) {
            System.out.print(i * number + " ");
        }
        System.out.println();

        /*
         * 
         * int variable = 0;
         * for(int i = 0; i < 10; i++) {
         * variable+=number;
         * System.out.println(variable);
         * }
         */
    }

    public void multiTables() {
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                System.out.print(i * j + " ");

            }
            System.out.println();
        }

        // version avec une seule boucle...
        // int i = 1;
        // int j = 1;
        // while(i <= 10 && j <= 10) {
        // System.out.print(i*j+" ");
        // i++;
        // if(i > 10) {
        // j++;
        // i = 1;
        // System.out.println();
        // }
        // }

    }

    public void pyramid(int height) {
        int starNb = 1;
        int spaceNb = height;
        for (int i = 1; i <= height; i++) {
            for(int j = 1; j < spaceNb; j++) {
                System.out.print(" ");

            }
            for(int j = 0; j < starNb; j++) {
                System.out.print("*");
            }
            System.out.println();
            spaceNb--;
            starNb += 2;
        }

        /*

        for (int i = 1; i <= height; i++) {
            
            System.out.print(" ".repeat(spaceNb));
            System.out.println("*".repeat(starNb));
            spaceNb--;
            starNb += 2;
        }
        */

        // for(int i = 1; i <= 10; i+=2) {
        // System.out.println(i);
        // }
    }
}
