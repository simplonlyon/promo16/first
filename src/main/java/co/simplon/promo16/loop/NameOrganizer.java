package co.simplon.promo16.loop;

import java.util.ArrayList;
import java.util.List;

import co.simplon.promo16.oop.Person;

public class NameOrganizer {

    private List<String> nameList;

    public NameOrganizer() {
        nameList = new ArrayList<>(List.of("Yousra", "Phonevilay", "Florian", "Roland", "Melissa", "Flo", "Magatte",
                "Melwic", "Aïcha", "Oualid", "Claire", "Kevin", "Khaled", "Rafaël", "Nolwenn"));

    }

    /**
     * Méthode qui renvoie une liste filtrée avec uniquement les prénoms commençant
     * par
     * la lettre donnée en argument
     * 
     * @param letter La lettre par laquelle le prénom doit commencer
     * @return La nouvelle liste filtrée
     */
    public List<String> getNamesStartingBy(String letter) {

        List<String> filteredNames = new ArrayList<>();
        for (String name : nameList) {
            if (name.toLowerCase().startsWith(letter.toLowerCase())) {

                filteredNames.add(name);
            }
        }
        return filteredNames;

    }

    /**
     * Message qui n'affiche que les prénoms avec un index impair
     */
    public void oddOnly() {
        for (int i = 1; i < nameList.size(); i += 2) {
            System.out.println(nameList.get(i));
        }

        // for(int i = 0; i < nameList.size(); i++) {
        // if(i % 2 != 0) {
        // System.out.println(nameList.get(i));
        // }
        // }
    }

    /**
     * Méthode qui affiche tous les prénoms et le premier et le dernier en
     * majuscules
     */
    public void StudentS() {
        for (int i = 0; i < nameList.size(); i++) {
            String name = nameList.get(i);
            if (i == 0 || i == nameList.size() - 1) {
                System.out.println(name.toUpperCase());
            } else {
                System.out.println(name);
            }
        }
    }

    public List<Person> promoMaker() {
        List<Person> persons = new ArrayList<>();

        for (String firstName : nameList) {
            Person person = new Person("P16", firstName);
            
            persons.add(person);
            
        }

        return persons;
    }

}
